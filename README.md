# 介绍

<img src="https://gitee.com/mengi/imgs/raw/master/img/cat.jpg" style="zoom: 40%;" />

- 本仓库是 wechat 小程序商城项目源码

# 使用

1. 安装

```bash
pnpm i
```

2. 运行

```bash
pnpm dev:mp-weixin
```

3. 在小程序开发者工具中，导入 dist 开发预览
