"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      cateList: [],
      selected: 0
    };
  },
  computed: {
    childCateList() {
      var _a;
      return (_a = this.cateList[this.selected]) == null ? void 0 : _a.children;
    }
  },
  onLoad() {
    this.getCates();
  },
  methods: {
    changeSelected(i) {
      console.log("\u5F53\u524D\u9009\u4E2D\u7236\u7EA7\u5206\u7C7B\u7684\u7D22\u5F15\uFF1A", i);
      this.selected = i;
    },
    async getCates() {
      const { data } = await this.request({ url: "/api/public/v1/categories" });
      console.log("\u6240\u6709\u5206\u7C7B\u6570\u636E\uFF1A", data);
      this.cateList = data;
    }
  }
};
if (!Array) {
  const _component_Search = common_vendor.resolveComponent("Search");
  _component_Search();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.cateList, (parent, i, i0) => {
      return {
        a: common_vendor.t(parent.cat_name),
        b: common_vendor.o(($event) => $options.changeSelected(i), parent.cat_id),
        c: $data.selected === i ? 1 : "",
        d: parent.cat_id
      };
    }),
    b: common_vendor.f($options.childCateList, (item, k0, i0) => {
      return {
        a: common_vendor.t(item.cat_name),
        b: common_vendor.f(item.children, (it, k1, i1) => {
          return {
            a: it.cat_icon,
            b: common_vendor.t(it.cat_name),
            c: "/packone/list/index?query=" + it.cat_name,
            d: it.cat_id
          };
        }),
        c: item.cat_id
      };
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-f0c2a821"], ["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/pages/category/index.vue"]]);
wx.createPage(MiniProgramPage);
