"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      options: [
        {
          text: "\u53D6\u6D88",
          style: {
            backgroundColor: "#007aff"
          }
        },
        {
          text: "\u5220\u9664",
          style: {
            backgroundColor: "#dd524d"
          }
        }
      ],
      carts: [],
      address: null
    };
  },
  onShow() {
    console.log("\u9875\u9762\u6253\u5F00\u8FD0\u884C\u4E86");
    this.carts = common_vendor.index.getStorageSync("carts") || [];
  },
  computed: {
    allAdress() {
      if (!this.address)
        return "";
      const {
        provinceName,
        cityName,
        countyName,
        detailInfo
      } = this.address;
      return provinceName + cityName + countyName + detailInfo;
    },
    isAll() {
      return this.seledGoods.length === this.carts.length;
    },
    seledGoods() {
      return this.carts.filter((item) => item.goods_checked);
    },
    seledGoodsTotal() {
      return this.seledGoods.reduce((acc, item) => acc += item.goods_count * item.goods_price, 0);
    }
  },
  watch: {
    carts: {
      deep: true,
      handler(newValue) {
        console.log("carts\u53D8\u5316\u4E86\uFF1A", newValue);
        common_vendor.index.setStorageSync("carts", newValue);
      }
    }
  },
  methods: {
    onClick(e, good) {
      console.log(e, good);
      if (e.index === 1) {
        common_vendor.index.showModal({
          title: "\u63D0\u793A",
          content: `\u786E\u8BA4\u5220\u9664: ${good.goods_name}\u5417\uFF1F`,
          showCancel: true,
          cancelText: "\u53D6\u6D88",
          cancelColor: "#000000",
          confirmText: "\u786E\u5B9A",
          confirmColor: "#3CC51F",
          success: (result) => {
            if (result.confirm) {
              this.carts = this.carts.filter((item) => item.goods_id !== good.goods_id);
            }
          },
          fail: () => {
          },
          complete: () => {
          }
        });
      }
    },
    async createOrder() {
      if (!this.address || !this.seledGoods.length) {
        return common_vendor.index.showToast({
          title: "\u8BF7\u9009\u62E9\u6536\u8D27\u5730\u5740\uFF0C\u540C\u65F6\u81F3\u5C11\u9009\u4E2D\u4E00\u4EF6\u5546\u54C1\u8FDB\u884C\u7ED3\u7B97",
          icon: "none"
        });
      }
      if (!common_vendor.index.getStorageSync("ugo-token-150")) {
        return common_vendor.index.navigateTo({
          url: "/packone/auth/index"
        });
      }
      console.log("\u521B\u5EFA\u8BA2\u5355");
      const { msg, data } = await this.request({
        url: "/api/public/v1/my/orders/create",
        method: "post",
        data: {
          order_price: this.seledGoodsTotal,
          consignee_addr: this.allAdress,
          goods: this.seledGoods.map((item) => {
            item.goods_number = item.goods_count;
            return item;
          })
        }
      });
      if (msg.status === 200) {
        this.carts = this.carts.filter((item) => item.goods_checked === false);
        console.log("\u672A\u9009\u4E2D\uFF1A", this.carts);
        common_vendor.index.showToast({
          title: "\u521B\u5EFA\u6210\u529F\uFF01"
        });
        common_vendor.index.navigateTo({
          url: "/packone/order/index"
        });
      } else {
        common_vendor.index.showToast({
          title: "\u521B\u5EFA\u5931\u8D25"
        });
      }
    },
    async getAdress() {
      try {
        const address = await common_vendor.index.chooseAddress();
        console.log("\u83B7\u53D6\u5730\u5740\u4FE1\u606F\uFF1A", address);
        this.address = address;
      } catch (error) {
        console.log(error);
      }
    },
    selAll() {
      if (this.isAll) {
        this.carts.forEach((item) => {
          item.goods_checked = false;
        });
      } else {
        this.carts.forEach((item) => {
          item.goods_checked = true;
        });
      }
    },
    singleCheck(index) {
      const good = this.carts[index];
      good.goods_checked = !good.goods_checked;
    },
    changeCount(index, step) {
      const currGoodCout = this.carts[index].goods_count;
      if (step === -1 && currGoodCout === 1) {
        common_vendor.index.showToast({
          title: "\u6700\u5C0F\u8D2D\u4E70\u6570\u91CF\u4E3A1"
        });
        return;
      }
      if (step === 1 && currGoodCout >= 6) {
        common_vendor.index.showToast({
          title: "\u6CA1\u6709\u5E93\u5B58\u4E86"
        });
        return;
      }
      this.carts[index].goods_count += step;
    }
  }
};
if (!Array) {
  const _easycom_uni_swipe_action_item2 = common_vendor.resolveComponent("uni-swipe-action-item");
  const _easycom_uni_swipe_action2 = common_vendor.resolveComponent("uni-swipe-action");
  (_easycom_uni_swipe_action_item2 + _easycom_uni_swipe_action2)();
}
const _easycom_uni_swipe_action_item = () => "../../node-modules/@dcloudio/uni-ui/lib/uni-swipe-action-item/uni-swipe-action-item.js";
const _easycom_uni_swipe_action = () => "../../node-modules/@dcloudio/uni-ui/lib/uni-swipe-action/uni-swipe-action.js";
if (!Math) {
  (_easycom_uni_swipe_action_item + _easycom_uni_swipe_action)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $data.address
  }, $data.address ? {
    b: common_vendor.t($data.address.userName),
    c: common_vendor.t($data.address.telNumber),
    d: common_vendor.t($options.allAdress)
  } : {
    e: common_vendor.o((...args) => $options.getAdress && $options.getAdress(...args))
  }, {
    f: common_vendor.f($data.carts, (good, i, i0) => {
      return {
        a: good.goods_small_logo,
        b: common_vendor.t(good.goods_name),
        c: common_vendor.t(good.goods_price),
        d: common_vendor.o(($event) => $options.changeCount(i, -1), good.goods_id),
        e: good.goods_count,
        f: common_vendor.o(($event) => $options.changeCount(i, 1), good.goods_id),
        g: good.goods_checked ? "#ea4451" : "#ccc",
        h: common_vendor.o(($event) => $options.singleCheck(i), good.goods_id),
        i: good.goods_id,
        j: common_vendor.o(($event) => $options.onClick($event, good), good.goods_id),
        k: "3277fd7b-1-" + i0 + ",3277fd7b-0"
      };
    }),
    g: common_vendor.p({
      ["right-options"]: $data.options
    }),
    h: $options.isAll ? "#ea4451" : "#ccc",
    i: common_vendor.o((...args) => $options.selAll && $options.selAll(...args)),
    j: common_vendor.t($options.seledGoodsTotal),
    k: common_vendor.t($options.seledGoods.length),
    l: common_vendor.o((...args) => $options.createOrder && $options.createOrder(...args))
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-3277fd7b"], ["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/pages/cart/index.vue"]]);
wx.createPage(MiniProgramPage);
