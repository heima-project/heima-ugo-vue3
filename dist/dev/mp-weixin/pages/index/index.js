"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      pageHeight: "auto",
      swiperList: [],
      cateList: [],
      floorList: [],
      isShowTop: false
    };
  },
  onLoad() {
    this.getSwiper();
    this.getCate();
    this.getFloor();
  },
  onReachBottom() {
    console.log("\u9875\u9762\u6EDA\u52A8\u5230\u5E95\u90E8\u7684\u65F6\u5019\u4F1A\u6267\u884C");
  },
  onPullDownRefresh() {
    console.log("\u5F00\u59CB\u4E0B\u62C9\u5237\u65B0\uFF1A");
    Promise.all([this.getSwiper(), this.getCate(), this.getFloor()]).then(() => {
      common_vendor.index.stopPullDownRefresh();
    });
  },
  onPageScroll(e) {
    if (e.scrollTop > common_vendor.index.getSystemInfoSync().windowHeight / 2) {
      this.isShowTop = true;
    } else {
      this.isShowTop = false;
    }
  },
  methods: {
    goTop() {
      common_vendor.index.pageScrollTo({
        scrollTop: 0,
        duration: 300
      });
    },
    async getSwiper() {
      const { data } = await this.request({
        url: "/api/public/v1/home/swiperdata"
      });
      console.log("\u8F6E\u64AD\u56FE\uFF1A", data);
      this.swiperList = data;
    },
    async getCate() {
      const { data } = await this.request({
        url: "/api/public/v1/home/catitems"
      });
      console.log("\u5BFC\u822A\uFF1A", data);
      this.cateList = data;
    },
    async getFloor() {
      const { data } = await this.request({
        url: "/api/public/v1/home/floordata"
      });
      this.floorList = data;
    },
    async testApi() {
      const res = await this.request({
        url: "/api/public/v1/home/swiperdata"
      });
      console.log("\u540E\u53F0\u6570\u636E\uFF1A", res);
    },
    disScroll(pageHeight) {
      console.log("\u641C\u7D22\u5B50\u7EC4\u4EF6\u8FDB\u5165\u641C\u7D22\u72B6\u6001\u4F20\u9012\u7684\u9875\u9762\u9AD8\u5EA6\uFF1A", pageHeight);
      this.pageHeight = pageHeight;
    }
  }
};
if (!Array) {
  const _component_Search = common_vendor.resolveComponent("Search");
  _component_Search();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.o($options.disScroll),
    b: common_vendor.f($data.swiperList, (item, k0, i0) => {
      return {
        a: item.image_src,
        b: `/packone/goods/index?id=${item.goods_id}`,
        c: item.goods_id
      };
    }),
    c: common_vendor.f($data.cateList, (item, i, i0) => {
      return {
        a: item.image_src,
        b: item.navigator_url ? "switchTab" : "navigate",
        c: item.navigator_url ? "/pages/category/index" : `/packone/list/index?query=${item.name}`,
        d: i
      };
    }),
    d: common_vendor.f($data.floorList, (item, i, i0) => {
      return {
        a: item.floor_title.image_src,
        b: common_vendor.f(item.product_list, (prd, k1, i1) => {
          return {
            a: prd.image_src,
            b: "/packone/list/index?query=" + prd.name,
            c: prd.name
          };
        }),
        c: i
      };
    }),
    e: $data.isShowTop
  }, $data.isShowTop ? {
    f: common_vendor.o((...args) => $options.goTop && $options.goTop(...args))
  } : {}, {
    g: $data.pageHeight
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/pages/index/index.vue"]]);
_sfc_main.__runtimeHooks = 1;
wx.createPage(MiniProgramPage);
