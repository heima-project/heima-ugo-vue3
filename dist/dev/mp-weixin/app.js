"use strict";
Object.defineProperties(exports, { __esModule: { value: true }, [Symbol.toStringTag]: { value: "Module" } });
const common_vendor = require("./common/vendor.js");
const utils_plugins = require("./utils/plugins.js");
const utils_mixins = require("./utils/mixins.js");
require("./utils/request.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/category/index.js";
  "./pages/cart/index.js";
  "./pages/profile/index.js";
  "./packone/goods/index.js";
  "./packone/list/index.js";
  "./packone/order/index.js";
  "./packone/auth/index.js";
}
const _sfc_main = {
  onLaunch: function() {
    console.log("App Launch");
  },
  onShow: function() {
    console.log("App Show");
  },
  onHide: function() {
    console.log("App Hide");
  }
};
const App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/App.vue"]]);
const Search = () => "./components/search.js";
function createApp() {
  const app = common_vendor.createSSRApp(App);
  app.component("Search", Search);
  app.mixin(utils_mixins.mixins);
  app.use(utils_plugins.plugins);
  return {
    app
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
