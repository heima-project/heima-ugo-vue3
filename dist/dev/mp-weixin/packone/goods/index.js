"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      detail: {},
      carts: common_vendor.index.getStorageSync("carts") || []
    };
  },
  onLoad(query) {
    console.log("\u8DEF\u7531\u4F20\u53C2\uFF1A", query);
    this.getDetail(query.id);
  },
  watch: {
    carts: {
      deep: true,
      handler(newValue) {
        console.log("carts\u53D8\u5316\u4E86\uFF1A", newValue);
        common_vendor.index.setStorageSync("carts", newValue);
      }
    }
  },
  methods: {
    addCart() {
      let goods = this.carts.find((item) => item.goods_id === this.detail.goods_id);
      if (!goods) {
        const { goods_id, goods_name, goods_price, goods_small_logo } = this.detail;
        this.carts.push({
          goods_id,
          goods_name,
          goods_price,
          goods_small_logo,
          goods_count: 1,
          goods_checked: true
        });
      } else {
        goods.goods_count++;
      }
      common_vendor.index.showToast({
        title: "\u52A0\u5165\u6210\u529F\uFF01"
      });
    },
    async getDetail(goods_id) {
      const { data } = await this.request({
        url: "/api/public/v1/goods/detail",
        data: {
          goods_id
        }
      });
      console.log("\u8BE6\u60C5\u6570\u636E\uFF1A", data);
      this.detail = data;
    },
    goCart() {
      common_vendor.index.switchTab({
        url: "/pages/cart/index"
      });
    },
    createOrder() {
      common_vendor.index.navigateTo({
        url: "/pages/order/index"
      });
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.f($data.detail.pics, (img, k0, i0) => {
      return {
        a: img.pics_big_url,
        b: img.pics_id
      };
    }),
    b: common_vendor.t($data.detail.goods_price),
    c: common_vendor.t($data.detail.goods_name),
    d: $data.detail.goods_introduce,
    e: common_vendor.o((...args) => $options.goCart && $options.goCart(...args)),
    f: $data.carts.length > 0
  }, $data.carts.length > 0 ? {
    g: common_vendor.t($data.carts.length)
  } : {}, {
    h: common_vendor.o((...args) => $options.addCart && $options.addCart(...args)),
    i: common_vendor.o((...args) => $options.createOrder && $options.createOrder(...args))
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-8406beb2"], ["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/packone/goods/index.vue"]]);
wx.createPage(MiniProgramPage);
