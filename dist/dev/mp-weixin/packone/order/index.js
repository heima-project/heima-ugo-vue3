"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      orders: []
    };
  },
  onLoad() {
    this.getOrders();
  },
  methods: {
    async payOrder(order_number) {
      const { msg, data } = await this.request({
        url: "/api/public/v1/my/orders/req_unifiedorder",
        method: "post",
        data: { order_number }
      });
      if (msg.status === 200) {
        try {
          const res = await common_vendor.index.requestPayment(data.pay);
          console.log("\u652F\u4ED8\u7ED3\u679C\u4FE1\u606F\uFF1A", res);
          common_vendor.index.showToast({
            title: "\u652F\u4ED8\u6210\u529F"
          });
        } catch (error) {
          common_vendor.index.showToast({
            title: "\u652F\u4ED8\u5931\u8D25"
          });
        }
      }
    },
    async getOrders() {
      if (!common_vendor.index.getStorageSync("ugo-token-150")) {
        return common_vendor.index.navigateTo({ url: "/packone/auth/index" });
      }
      let { msg, data } = await this.request({
        url: "/api/public/v1/my/orders/all",
        data: {
          type: 1
        }
      });
      if (msg.status === 200) {
        this.orders = data.orders;
      }
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.orders, (order, k0, i0) => {
      return {
        a: common_vendor.f(order.goods, (prd, k1, i1) => {
          return {
            a: prd.goods_small_logo,
            b: common_vendor.t(prd.goods_name),
            c: common_vendor.t(prd.goods_price),
            d: common_vendor.t(prd.goods_number),
            e: prd.goods_id
          };
        }),
        b: common_vendor.t(order.goods.length),
        c: common_vendor.t(order.total_price),
        d: common_vendor.t(order.order_number),
        e: common_vendor.o(($event) => $options.payOrder(order.order_number), order.order_number),
        f: order.order_number
      };
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-de175347"], ["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/packone/order/index.vue"]]);
wx.createPage(MiniProgramPage);
