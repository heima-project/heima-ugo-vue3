"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      triggered: false,
      list: [],
      total: 0,
      query: {
        query: "",
        pagenum: 1,
        pagesize: 20
      }
    };
  },
  onLoad(params) {
    console.log("\u641C\u7D22\u5173\u952E\u8BCD\uFF1A", params);
    this.query.query = params.query;
    this.getList(this.query);
  },
  computed: {
    hasMore() {
      return this.list.length === this.total;
    }
  },
  methods: {
    async refresh() {
      this.triggered = true;
      this.list = [];
      this.query.pagenum = 1;
      await this.getList(this.query);
      this.triggered = false;
    },
    getMore() {
      console.log("\u5217\u8868\u5230\u5E95\u90E8\u4E86");
      if (this.hasMore)
        return;
      this.query.pagenum++;
      this.getList(this.query);
    },
    async getList(params) {
      const { data } = await this.request({
        url: "/api/public/v1/goods/search",
        data: params
      });
      this.total = data.total;
      this.list.push(...data.goods);
    },
    goDetail(id) {
      common_vendor.index.navigateTo({
        url: `/packone/goods/index?id=${id}`
      });
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.f($data.list, (item, k0, i0) => {
      return {
        a: item.goods_small_logo,
        b: common_vendor.t(item.goods_name),
        c: common_vendor.t(item.goods_price),
        d: item.goods_id,
        e: common_vendor.o(($event) => $options.goDetail(item.goods_id), item.goods_id)
      };
    }),
    b: $options.hasMore
  }, $options.hasMore ? {} : {}, {
    c: common_vendor.o((...args) => $options.getMore && $options.getMore(...args)),
    d: $data.triggered,
    e: common_vendor.o((...args) => $options.refresh && $options.refresh(...args))
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-93b59b6f"], ["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/packone/list/index.vue"]]);
wx.createPage(MiniProgramPage);
