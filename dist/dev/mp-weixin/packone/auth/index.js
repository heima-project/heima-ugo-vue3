"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  onLoad() {
    this.getCode();
  },
  methods: {
    async getCode() {
      try {
        const codeData = await common_vendor.index.login();
        this.code = codeData.code;
      } catch (error) {
        console.log(error);
      }
    },
    async login() {
      try {
        const { encryptedData, rawData, iv, signature } = await common_vendor.index.getUserProfile({ desc: "\u83B7\u53D6\u5FAE\u4FE1\u7528\u6237\u4FE1\u606F\u767B\u5F55" });
        console.log("\u5FAE\u4FE1\u7528\u6237\u4FE1\u606F\uFF1A", rawData);
        const { msg, data } = await this.request({
          url: "/api/public/v1/users/wxlogin",
          method: "post",
          data: {
            encryptedData,
            rawData,
            iv,
            signature,
            code: this.code
          }
        });
        console.log("\u767B\u5F55\uFF1A", msg, data);
        if (msg.status === 200) {
          common_vendor.index.setStorageSync("ugo-token-150", data.token);
          common_vendor.index.navigateBack();
        } else {
          common_vendor.index.showToast({
            title: "\u767B\u5F55\u5931\u8D25\uFF01"
          });
        }
      } catch (error) {
        console.log(error);
      }
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.login && $options.login(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-3e5ba2f8"], ["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/packone/auth/index.vue"]]);
wx.createPage(MiniProgramPage);
