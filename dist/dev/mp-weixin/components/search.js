"use strict";
const common_vendor = require("../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      isSearch: false,
      keyWord: "",
      suggestList: [],
      timer: null,
      history: common_vendor.index.getStorageSync("history") || []
    };
  },
  watch: {
    history(newValue) {
      common_vendor.index.setStorageSync("history", newValue);
    }
  },
  methods: {
    goList() {
      common_vendor.index.navigateTo({
        url: `/packone/list/index?query=${this.keyWord}`
      });
      if (!this.history.some((item) => item === this.keyWord)) {
        this.history.push(this.keyWord);
      }
      this.keyWord = "";
      this.suggestList = [];
    },
    clearHistory() {
      this.history = [];
      common_vendor.index.removeStorageSync("history");
    },
    getSuggestList() {
      this.timer && clearTimeout(this.timer);
      this.timer = setTimeout(async () => {
        if (!this.keyWord)
          return this.suggestList = [];
        const { data } = await this.request({
          url: "/api/public/v1/goods/qsearch",
          data: {
            query: this.keyWord
          }
        });
        console.log("\u5EFA\u8BAE\u5546\u54C1\u5217\u8868\uFF1A", data);
        this.suggestList = data;
      }, 600);
    },
    goSearch() {
      this.isSearch = true;
      common_vendor.index.hideTabBar({ animation: true });
      console.log("\u624B\u673A\u4FE1\u606F\uFF1A", common_vendor.index.getSystemInfoSync());
      const pageHeight = common_vendor.index.getSystemInfoSync().windowHeight + "px";
      this.$emit("search", pageHeight);
    },
    exitSearch() {
      this.isSearch = false;
      common_vendor.index.showTabBar({ animation: true });
      this.$emit("search", "auto");
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.o((...args) => $options.goSearch && $options.goSearch(...args)),
    b: common_vendor.o([($event) => $data.keyWord = $event.detail.value, (...args) => $options.getSuggestList && $options.getSuggestList(...args)]),
    c: common_vendor.o((...args) => $options.goList && $options.goList(...args)),
    d: $data.keyWord,
    e: common_vendor.o((...args) => $options.exitSearch && $options.exitSearch(...args)),
    f: $data.suggestList.length === 0
  }, $data.suggestList.length === 0 ? {
    g: common_vendor.o((...args) => $options.clearHistory && $options.clearHistory(...args)),
    h: common_vendor.f($data.history, (item, k0, i0) => {
      return {
        a: common_vendor.t(item),
        b: item,
        c: `/packone/list/index?query=${item}`
      };
    })
  } : {
    i: common_vendor.f($data.suggestList, (item, k0, i0) => {
      return {
        a: common_vendor.t(item.goods_name),
        b: `/packone/goods/index?id=${item.goods_id}`,
        c: item.goods_id
      };
    })
  }, {
    j: $data.isSearch,
    k: $data.isSearch ? 1 : ""
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-146e7ef3"], ["__file", "/Users/work/itcast\u8BFE\u7A0B/applet\u5C0F\u7A0B\u5E8F/code/ugo-vue3/src/components/search.vue"]]);
wx.createComponent(Component);
