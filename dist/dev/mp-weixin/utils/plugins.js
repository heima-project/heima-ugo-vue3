"use strict";
const utils_request = require("./request.js");
const plugins = {
  install(app) {
    app.config.globalProperties.request = utils_request.request;
  }
};
exports.plugins = plugins;
