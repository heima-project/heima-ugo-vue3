"use strict";
const common_vendor = require("../common/vendor.js");
const BASE_URL = "https://api-hmugo-web.itheima.net";
const request = async ({ url, method, header = {}, data }) => {
  common_vendor.index.showLoading({
    title: "\u8BF7\u6C42\u4E2D...",
    mask: true
  });
  const token = common_vendor.index.getStorageSync("ugo-token-150");
  if (token) {
    header.Authorization = token;
  }
  try {
    const res = await common_vendor.index.request({
      url: BASE_URL + url,
      method,
      header,
      data
    });
    if (res.statusCode >= 200 && res.statusCode < 300) {
      return {
        msg: res.data.meta,
        data: res.data.message
      };
    } else {
      return Promise.reject(error);
    }
  } catch (error2) {
    console.log("request:", error2);
    return Promise.reject(error2);
  } finally {
    common_vendor.index.hideLoading();
  }
};
exports.request = request;
