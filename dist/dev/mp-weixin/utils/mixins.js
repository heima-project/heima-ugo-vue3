"use strict";
const common_vendor = require("../common/vendor.js");
const mixins = {
  data() {
    return {
      meng: 1e3
    };
  },
  methods: {
    checkAuth() {
      const token = common_vendor.index.getStorageSync("ugo-token-150");
      if (!token) {
        common_vendor.index.navigateTo({
          url: "/packone/auth/index"
        });
      }
    }
  }
};
exports.mixins = mixins;
