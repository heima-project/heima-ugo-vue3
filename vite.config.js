import { defineConfig } from 'vite'
import uni from '@dcloudio/vite-plugin-uni'
// https://vitejs.dev/config/
export default defineConfig({
  optimizeDeps: ['@dcloudio/uni-ui'],
  plugins: [
    uni(),
  ],
})
