import {
	createSSRApp
} from "vue";
import App from "./App.vue";

// 1. 导入搜索组件
import Search from '@/components/search.vue'

// 导入插件
import plugins from '@/utils/plugins'

// 导入全局混入对象
import mixins from '@/utils/mixins'

export function createApp () {
	const app = createSSRApp(App);
	app.component('Search', Search);
	app.mixin(mixins);
	app.use(plugins)
	return {
		app,
	};
}
