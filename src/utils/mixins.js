/**
 * vue全局混入
 * 语法：{组件的选项}
 */
export default {
  data () {
    return {
      meng: 1000
    }
  },
  methods: {
    /**
     * 检查用户是否有权限访问某个页面
     * 核心：根据是否有token
     * 1. 有token：正常访问
     * 2. 无token：跳回登录页
     */
    checkAuth () {
      const token = uni.getStorageSync('ugo-token-150')
      if (!token) {
        // 没有token,跳回登录页
        uni.navigateTo({
          url: '/packone/auth/index'
        })
      }
    }
  },
}
